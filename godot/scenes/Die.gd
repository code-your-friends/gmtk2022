extends RigidBody

const Game = preload("res://scenes/Game.gd")

var camrot_h = 0
var camrot_v = 0
var camrot_v_locked_val = 0
var cam_v_min = -90
var cam_v_max = 90
var sensitivity_cam = 0.5
var sensitivity_charge = 2
var dice_is_moving = true
var dice_is_still_since = 0
var dice_still_timing = false
var last_frame_position = Vector3(1.0,1.0,1.0)
var mouse_origin = Vector2(0,0)
var die_launch_force_magnitude = 0
var die_launch_force_direction = Vector3(0,0,0)
var die_launch_force = Vector3(0,0,0)
var die_launch_force_multiplier = 0.05
var die_launch_force_magnitude_max = 1000
var left_pressed = false
var mat
var target_bounce = 1
var default_gravity = 2
var default_bounciness = 0
var after_stroke = true
var plus1Start = 999999999999

var curr_buff = buff.none

var _game: Game = null

enum buff {
	none,
	stroke,
	bounce,
	phase,
	gravity,
	ball
}

# Called when the node enters the scene tree for the first time.
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	$CamRoot/Horizontal/Vertical/Camera.add_exception(self)
	$CamRoot.set_as_toplevel(true)
	$PowerUI3D.set_as_toplevel(true)
	_game = get_node("/root/Game")
	mat = get_physics_material_override()
	mat.friction = 1
	gravity_scale = default_gravity
	$PowerUI3D/Arrow.hide()
	$PowerUI/PowerBar.hide()
	$PowerUI/PowerBar.max_value = die_launch_force_magnitude_max


func _input(event):
	
	if _game.is_post_game():
		if event is InputEventMouseButton || event is InputEventKey:
			_game.next_level()
			translation.x = 0
			translation.y = 0
			translation.z = 0
			return
	
	if event is InputEventKey:
		if event.scancode == KEY_TAB:
			if event.is_pressed():
				_game.open_scoreboard()
			else:
				_game.close_scoreboard()
		return

	if event is InputEventMouseButton and dice_is_moving == false:
		if event.is_pressed() and left_pressed == false:
			left_pressed = true
			camrot_v_locked_val = camrot_v
			#Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			#mouse_origin = event.global_position
			die_launch_force_magnitude = 0
			$PowerUI/PowerBar.show()
			draw_arrow()
			$PowerUI3D/Arrow.show()

		else:
			left_pressed = false
			#die_launch_force_magnitude = -(mouse_origin.y - event.global_position.y)
			die_launch_force_direction = $CamRoot/Horizontal/Vertical/Camera.get_global_transform().basis
			die_launch_force_direction = -die_launch_force_direction.z
			die_launch_force = die_launch_force_direction * clamp(die_launch_force_magnitude, 0, die_launch_force_magnitude_max) * die_launch_force_multiplier
			self.apply_central_impulse(die_launch_force)
			if curr_buff != buff.ball:
				self.apply_torque_impulse(die_launch_force)
			$PowerUI/PowerBar.hide()
			$PowerUI3D/Arrow.hide()
			_game.add_stroke()
			after_stroke = true
			#Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
			# add one to counter
	
	if event is InputEventMouseMotion:
		camrot_h += -event.relative.x * sensitivity_cam
		if left_pressed:
			camrot_v = camrot_v_locked_val
			die_launch_force_magnitude += event.relative.y * sensitivity_charge
			$PowerUI/PowerBar.value = die_launch_force_magnitude
			draw_arrow()
		else:
			camrot_v += -event.relative.y * sensitivity_cam
			

func _physics_process(delta):
	camrot_v = clamp(camrot_v, cam_v_min, cam_v_max)
	
	$CamRoot/Horizontal.rotation_degrees.y = camrot_h
	$CamRoot/Horizontal/Vertical.rotation_degrees.x = camrot_v
	$CamRoot.translation.x = translation.x 
	$CamRoot.translation.y = translation.y + 5
	$CamRoot.translation.z = translation.z
	$PowerUI3D.translation.x = translation.x 
	$PowerUI3D.translation.y = translation.y
	$PowerUI3D.translation.z = translation.z
	
	if dice_is_moving == true and dice_still_timing == false and angular_velocity.length() < 0.1 and linear_velocity.length() < 0.1:
		print("stop morbin and start timing")
		dice_is_still_since = OS.get_ticks_msec()
		dice_still_timing = true
		
	elif dice_is_moving == false and after_stroke and (angular_velocity.length() >= 0.1 or linear_velocity.length() >= 0.1):
		print("start morbin")
		dice_still_timing = false
		dice_is_moving = true
		$PowerUI3D/SpriteCircles.hide()
		
	if after_stroke && dice_still_timing == true && (OS.get_ticks_msec() - dice_is_still_since) > 800:
		print("stop timing while not morbin")
		dice_is_moving = false
		after_stroke = false
		dice_still_timing = false
		$PowerUI3D/SpriteCircles.show()
		revert_current_buff()
		var last_buff = curr_buff
		curr_buff = get_buff_from_upwards_side()
		if not(last_buff == curr_buff && curr_buff == buff.stroke):
			apply_buff()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var rot = $CamRoot/Horizontal/Vertical/Camera.get_global_transform().basis.get_euler()
	$PowerUI3D.rotation_degrees.y = rad2deg(rot.y) + 180
	
	var remaining = clamp(1000 - (OS.get_ticks_msec() - plus1Start), 0, 1000)
	$PowerUI3D/SpritePlus1.translation.y = 2 + (1000-remaining)/300.0
	print(remaining)
	if remaining > 0 && remaining < 1000:
		$PowerUI3D/SpritePlus1.show()
	else:
		$PowerUI3D/SpritePlus1.hide()


func draw_arrow():
	var l = 1 + 5 * clamp(die_launch_force_magnitude, 0, die_launch_force_magnitude_max) / die_launch_force_magnitude_max
	var vertices = PoolVector3Array([
		# base left
		Vector3( 0.2, 0, 0),
		Vector3( 0.2, 0, l),
		Vector3(-0.2, 0, l),
		# base right
		Vector3( 0.2, 0, 0),
		Vector3(-0.2, 0, l),
		Vector3(-0.2, 0, 0),
		# tip
		Vector3( 0.4, 0, l),
		Vector3(-0.0, 0, l + 0.4),
		Vector3(-0.4, 0, l),
	])
	var st = SurfaceTool.new()
	var m = SpatialMaterial.new()
	var color = Color(0.9, 0.1, 0.1)
	m.albedo_color = color
	st.begin(Mesh.PRIMITIVE_TRIANGLES)
	st.set_material(m)
	for v in vertices.size(): 
		st.add_color(color)
		st.add_uv(Vector2(0,0))
		st.add_vertex(vertices[v])
	var tmpMesh = Mesh.new()
	st.commit(tmpMesh)
	$PowerUI3D/Arrow.mesh = tmpMesh


"""
BUFFS
"""

func is_on_side():
	if $Plus1.get_overlapping_areas().size() > 0 || $Plus2.get_overlapping_areas().size() > 0 || $Ball1.get_overlapping_areas().size() > 0 || $Ball2.get_overlapping_areas().size() > 0 || $Bounce1.get_overlapping_areas().size() > 0  || $Bounce2.get_overlapping_areas().size() > 0 || $Phase1.get_overlapping_areas().size() > 0 || $Phase2.get_overlapping_areas().size() > 0:
		return true
	else:
		return false

func get_buff_from_upwards_side():
	if $Plus1.get_overlapping_areas().size() > 0 || $Plus2.get_overlapping_areas().size() > 0:
		return buff.stroke
	if $Ball1.get_overlapping_areas().size() > 0 || $Ball2.get_overlapping_areas().size() > 0:
		return buff.ball
	if $Bounce1.get_overlapping_areas().size() > 0 || $Bounce2.get_overlapping_areas().size() > 0:
		return buff.bounce
	if $Phase1.get_overlapping_areas().size() > 0 || $Phase2.get_overlapping_areas().size() > 0:
		return buff.gravity
		
	return buff.none


func low_gravity():
	gravity_scale = 0.3 * default_gravity
	
func low_gravity_revert():
	gravity_scale = default_gravity

func extra_stroke():
	_game.revoke_stroke()
	plus1Start = OS.get_ticks_msec()

func bounciness():
	mat.bounce = target_bounce


func bounciness_revert():
	mat.bounce = default_bounciness


func phase():
	set_collision_layer_bit(2, false)


func phase_revert():
	set_collision_layer_bit(2, true)


func ball():
	$BallShape.show()
	$BallShape.set_process(true)
	$BallShape.disabled = false
	angular_velocity = Vector3(0,0,0)
	linear_velocity = Vector3(0,0,0)


func ball_revert():
	$BallShape.hide()
	$BallShape.set_process(false)
	$BallShape.disabled = true

func buff_text():
	match curr_buff:
		buff.ball:
			return ""
		buff.bounce:
			return "You're bouncy!"
		buff.phase:
			return "WooOOoOO you can phase through walls"
		buff.gravity:
			return "Warning, Low gravity!"
	return ""

func revert_current_buff():
	$PowerUI/LabelBuff.text = ""
	match curr_buff:
		buff.ball:
			ball_revert()
		buff.bounce:
			bounciness_revert()
		buff.phase:
			phase_revert()
		buff.gravity:
			low_gravity_revert()


func apply_buff():
	$PowerUI/LabelBuff.text = buff_text()
	match curr_buff:
		buff.ball:
			ball()
		buff.bounce:
			bounciness()
		buff.phase:
			phase()
		buff.stroke:
			extra_stroke()
		buff.gravity:
			low_gravity()
