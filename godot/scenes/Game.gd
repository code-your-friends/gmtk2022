extends Spatial

const NUM_LEVELS = 3
const PAR = [5,8,15]

# level control
var current_level_id = 0
var post_game = false

# stroke control
var current_strokes = 0
var strokes_per_level = [0,0,0]

var scenes = [
	preload("res://scenes/levels/level1/level1.tscn"), 
	preload("res://scenes/levels/level2/level2.tscn"),
	preload("res://scenes/levels/level3/level3.tscn")
	]

# Called when the node enters the scene tree for the first time.
func _ready():
	current_strokes = 0
	$JinglePlayer.stream.loop_mode = AudioStreamSample.LOOP_DISABLED
	load_scene_by_index(0)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		close_scoreboard()
		get_tree().paused = true
		$PausePopup.show()
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		


func _on_QuitButton_pressed():
	get_tree().quit()


func _on_ResumeButton_pressed():
	$PausePopup.hide()
	get_tree().paused = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)


func _on_MainMenuButton_pressed():
	get_tree().paused = false
	get_tree().change_scene("res://scenes/levels/MainMenu.tscn")


func end_level():
	open_scoreboard()
	evaluate_player(current_strokes, PAR[current_level_id])
	$BGMPLayer.stream_paused = true
	$JinglePlayer.play()
	post_game = true


func add_stroke():
	current_strokes += 1


func revoke_stroke():
	current_strokes -= 1


func open_scoreboard():
	strokes_per_level[current_level_id] = current_strokes
	$Scoreboard.update_values(strokes_per_level, PAR)
	$Scoreboard.show()


func close_scoreboard():
	$Scoreboard.hide()


func is_post_game():
	return post_game


func next_level():
	post_game = false

	current_level_id += 1
	current_strokes = 0
	
	if current_level_id >= NUM_LEVELS:
		get_tree().change_scene("res://scenes/levels/MainMenu.tscn")
		return 
	
	load_scene_by_index(current_level_id)
	
	close_scoreboard()
	$Evaluation.hide()
	
	$BGMPLayer.stream_paused = false
	$JinglePlayer.stop()


func evaluate_player(strokes, par):
	if strokes <= 1:
		$Evaluation/EvaluationLabel.text = "HOLE IN ONE"
		$Evaluation.show()
		return 
	
	var diff = strokes - par
	if diff < -2:
		$Evaluation/EvaluationLabel.text = "%d" % diff
	elif diff == -2:
		$Evaluation/EvaluationLabel.text = "EAGLE"
	elif diff == -1: 
		$Evaluation/EvaluationLabel.text = "BIRDIE"
	elif diff == 0:
		$Evaluation/EvaluationLabel.text = "PAR"
	elif diff == 1:
		$Evaluation/EvaluationLabel.text = "BOGEY"
	elif diff == 2:
		$Evaluation/EvaluationLabel.text = "DOUBLE BOGEY"
	else:
		$Evaluation/EvaluationLabel.text = "+ %d" % diff
	
	$Evaluation.show()


func _on_JinglePlayer_finished():
	$BGMPLayer.stream_paused = false


func load_scene_by_index(index):
	# clear level
	for n in $LoadedLevel.get_children():
		$LoadedLevel.remove_child(n)
		n.queue_free()
	
	# add new level
	$LoadedLevel
	var instance = scenes[index].instance()
	$LoadedLevel.add_child(instance)
