extends Spatial

var sum_delta = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(delta):
	sum_delta += delta
	global_rotate(Vector3.UP, delta)
	global_rotate(Vector3.FORWARD, -delta)
