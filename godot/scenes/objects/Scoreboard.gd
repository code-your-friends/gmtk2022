extends Control

func update_values(values, pars):
	$Panel/GridContainer/Player0.text = "%d" % values[0]
	$Panel/GridContainer/Player1.text = "%d" % values[1]
	$Panel/GridContainer/Player2.text = "%d" % values[2]
	
	var sum = 0
	for i in values:
		sum += i
	$Panel/GridContainer/PlayerSum.text = "%d" % sum
	
	$Panel/GridContainer/Par0.text = "%d" % pars[0]
	$Panel/GridContainer/Par1.text = "%d" % pars[1]
	$Panel/GridContainer/Par2.text = "%d" % pars[2]
	
	sum = 0
	for i in pars:
		sum += i
		
	$Panel/GridContainer/ParSum.text = "%d" % sum
