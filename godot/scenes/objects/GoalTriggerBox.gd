extends Area

var loader


func _on_GoalTriggerZone_tree_entered():
	loader = get_node("/root/Game")


func _on_GoalTriggerZone_body_entered(body :RigidBody):
	# return when body is not a rigidbody
	if body == null:
		return
	
	loader.end_level()
