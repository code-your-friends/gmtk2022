use gdnative::api::*;
use gdnative::prelude::*;
use crate::buff_trait::Buff;

pub struct BuffBall {
    name: String,
    description: String,
    collision_die: Ref<CollisionShape>,
    collision_sphere: Ref<CollisionShape>,
    mesh_die: Ref<MeshInstance>,
    mesh_sphere: Ref<MeshInstance>,
}

impl BuffBall {

    pub fn new(die_body: Box<Ref<RigidBody>>) -> Self {

        // find a collision shape
        fn search<T: SubClass<Node>> (die_body: &Box<Ref<RigidBody>>, name: String) -> Option<Ref<T>> {
            unsafe{
            match die_body.assume_safe().get_node(&name) {
                None => { 
                    godot_warn!("Could not find {}", name); 
                    None
                }
                Some(node) => {
                    match node.assume_safe().cast::<T>() {
                        None => {
                            godot_warn!("{} is not a {}", name, std::any::type_name::<T>());
                            None
                        },
                        Some(cs) => Some(cs.claim())
                    }
                }
            }
            }
        }

        BuffBall {
            name: String::from("Ball"),
            description: String::from("Roll the dice"),
            collision_die:    search::<CollisionShape>(&die_body, String::from("CollisionShapeDie"   )).unwrap(),
            collision_sphere: search::<CollisionShape>(&die_body, String::from("CollisionShapeSphere")).unwrap(),
            mesh_die:    search::<MeshInstance>(&die_body, String::from("MeshDie"   )).unwrap(),
            mesh_sphere: search::<MeshInstance>(&die_body, String::from("MeshSphere")).unwrap(),
        }
    }
}

impl Buff for BuffBall {
    unsafe fn execute_buff(&mut self) {
        self.collision_die.assume_safe().set_disabled(true);
        self.collision_sphere.assume_safe().set_disabled(false);
        self.mesh_die.assume_safe().set_visible(false);
        self.mesh_sphere.assume_safe().set_visible(true);
        godot_print!("Ball activated");
    }

    unsafe fn revert_buff(&mut self) {
        self.collision_die.assume_safe().set_disabled(false);
        self.collision_sphere.assume_safe().set_disabled(true);
        self.mesh_sphere.assume_safe().set_visible(false);
        self.mesh_die.assume_safe().set_visible(true);
        godot_print!("Ball deactivated");
    }

    fn get_name(self) -> GodotString {
        GodotString::from_str(self.name)
    }

    fn get_description(self) -> GodotString {
        GodotString::from_str(self.description)
    }
}
