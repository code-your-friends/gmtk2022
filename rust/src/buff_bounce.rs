use gdnative::api::*;
use gdnative::prelude::*;
use crate::buff_trait::Buff;

pub struct BuffBounce {
    name: String,
    description: String,
    rigid_body: Box<Ref<RigidBody>>,

    target_bounciness: f64,
    previous_bounciness: f64
}

impl BuffBounce {
    pub fn new(rigid_body: Box<Ref<RigidBody>>) -> Self {
        BuffBounce {
            name: String::from("Bounce"),
            description: String::from("Let's the die bounce more than usual."),
            rigid_body,
            target_bounciness: 1.0,
            previous_bounciness: 0.0
        }
    }
}

impl Buff for BuffBounce {

    unsafe fn execute_buff(&mut self) {
        // get the physics material
        match self.rigid_body.assume_safe().physics_material_override() {
            Some(mat) => {
                let save_mat = mat.assume_safe();
                self.previous_bounciness = save_mat.bounce();
                save_mat.set_bounce(self.target_bounciness);
            },
            None => godot_warn!("Physics material was not found")
        }
        godot_print!("Bounce activated");
    }

    unsafe fn revert_buff(&mut self) {
        // get the physics material
        match self.rigid_body.assume_safe().physics_material_override() {
            Some(mat) => {
                let save_mat = mat.assume_safe();
                save_mat.set_bounce(self.previous_bounciness);
            },
            None => godot_warn!("Physics material was not found")
        }
        godot_print!("Bounce deactivated");
    }

    fn get_name(self) -> GodotString {
        GodotString::from_str(self.name)
    }

    fn get_description(self) -> GodotString {
        GodotString::from_str(self.description)
    }
}
