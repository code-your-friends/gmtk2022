use gdnative::api::*;
use gdnative::prelude::*;

pub trait Buff {
    unsafe fn execute_buff(&mut self);
    unsafe fn revert_buff(&mut self);
    fn get_name(self) -> GodotString;
    fn get_description(self) -> GodotString;
}
