use gdnative::api::*;
use gdnative::prelude::*;
use gdnative::core_types::VariantArray;
use gdnative::object::*;

/// The SpinningCube "class"
#[derive(NativeClass)]
#[inherit(Node)]
pub struct GoalTriggerZone {
    level_loader: Option<Ref<Node>>,
}

#[methods]
impl GoalTriggerZone {
    fn new(_owner: &Node) -> Self {
        GoalTriggerZone {
            level_loader: None,
        }
    }

    #[export]
    unsafe fn _ready(&mut self, owner: &Node) {
        // TODO get loader
    }

    #[export]
    fn overlap(&mut self, owner: &Node) {
        godot_print!("OVERLAPPED");
    }
}
