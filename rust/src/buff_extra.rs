use gdnative::api::*;
use gdnative::prelude::*;
use crate::buff_trait::Buff;

pub struct BuffExtra {
    name: String,
    description: String,
}

impl BuffExtra {
    pub fn new() -> Self {
        BuffExtra {
            name: String::from("Extra Stroke"),
            description: String::from("One additional stroke that doesn't count"),
        }
    }
}

impl Buff for BuffExtra {
    unsafe fn execute_buff(&mut self) {
        todo!()
    }

    unsafe fn revert_buff(&mut self) {
        todo!()
    }

    fn get_name(self) -> GodotString {
        GodotString::from_str(self.name)
    }

    fn get_description(self) -> GodotString {
        GodotString::from_str(self.description)
    }
}
