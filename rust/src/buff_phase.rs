use gdnative::api::*;
use gdnative::prelude::*;
use crate::buff_trait::Buff;

pub struct BuffPhase {
    name: String,
    description: String,
    rigid_body: Box<Ref<RigidBody>>,

    wall_layer_bit: i64,
}

impl BuffPhase {
    pub fn new(rigid_body: Box<Ref<RigidBody>>) -> Self {
        // calculate the bit for the mask to enable/ disable collision detection
        let mask_layer = 2;
        let mask_bit = 2_i64.pow(mask_layer - 1);

        BuffPhase {
            name: String::from("Phase"),
            description: String::from("Phases through thin fences"),
            rigid_body,
            wall_layer_bit: mask_bit,
        }
    }
}

impl Buff for BuffPhase {
    unsafe fn execute_buff(&mut self) {
        // actually disable the collision to the fences
        self.rigid_body.assume_safe().set_collision_mask_bit(self.wall_layer_bit, false);
    }

    unsafe fn revert_buff(& mut self) {
        self.rigid_body.assume_safe().set_collision_mask_bit(self.wall_layer_bit, false);
    }

    fn get_name(self) -> GodotString {
        GodotString::from_str(self.name)
    }

    fn get_description(self) -> GodotString {
        GodotString::from_str(self.description)
    }
}
